# #!/usr/bin/env python

import os
import sys
import time
import multiprocessing as mp
import pandas as pd


# may be changed depending on filesize
FOLDER = 'logs'
LOG_EXTENSION = '.in'
CHUNK_SIZE = 1000000
CPU_COUNT = 4
# CPU_COUNT = mp.cpu_count() # this is slower on files with 30000 requests

# should not be changed
EVENTS = ('BackendRequest', 'BackendOk', 'StartMerge', 'FinishRequest')
COLUMNS = ('time', 'n_req', 'event', 'gr', 'info')
USED_COLUMNS = ('time', 'n_req', 'event')
DROP_COLUMNS = ('event')


def process_file(f: str) -> list:
    '''Process log file for calculation of 95th percentile and number of bad requests.
    '''
    start_time = time.time()
    pool = mp.Pool(CPU_COUNT)
    chunker = pd.read_csv(f,
                        chunksize=CHUNK_SIZE,
                        sep='\t',
                        header=None,
                        names=COLUMNS, 
                        usecols=USED_COLUMNS,
                        na_values='',
                        engine='c')

    df = pd.concat(pool.map(process_chunk, chunker))
    percentile = calc_percentile(df)
    n_bads, n_reqs = calc_bad_requests(df)
    calc_time = time.time() - start_time

    return [percentile, n_bads, n_reqs, calc_time]


def process_chunk(df: pd.DataFrame) -> pd.DataFrame:
    '''Cuts unused events from DataFrame
    '''
    df = df[df['event'].isin(EVENTS)]
    return df


def calc_percentile(df: pd.DataFrame) -> float:
    '''Calculates 95th percentile in seconds.
    Makes 2 dataframes with `StartMerge` event and `FinishRequest` event and 
    substracts first from second by `n_req` field.
    '''
    df_start = df.loc[df['event'] == 'StartMerge']
    df_end =  df.loc[df['event'] == 'FinishRequest']
    df_start = df_start.drop(columns=DROP_COLUMNS, axis=1)
    df_end = df_end.drop(columns=DROP_COLUMNS, axis=1)
    df_diff = (df_end.set_index('n_req')-df_start.set_index('n_req')).dropna(axis=0)
    percentile = df_diff['time'].quantile(q=0.95)
    return percentile 

def calc_bad_requests(df: pd.DataFrame) -> list:
    '''Calculates requests without response.
    Makes 2 dataframes with `BackendRequest` event and `BackendOk` event,
    then makes diff between them.
    '''
    # this option is set to avoid SettingWithCopyWarning
    pd.options.mode.chained_assignment = None
    df_req = df.loc[df['event'] == 'BackendRequest']
    df_ok = df.loc[df['event'] == 'BackendOk']
    df_req.drop_duplicates(subset=['n_req'], keep='first', inplace=True)
    df_ok.drop_duplicates(subset=['n_req'], keep='first', inplace=True)
    df_diff = df_req[~df_req['n_req'].isin(df_ok['n_req'])]

    return [len(df_diff), len(df_req)]


def get_files(argv, folder=FOLDER):
    '''Check args for path. If it exists, checks if it directory or file
    '''
    p = folder
    for arg in argv:
        if arg.split('=')[0] == 'path' and len(arg) > 1:
            p = arg.split('=')[1]
            break
    if os.path.isdir(p):
        return [os.path.join(p, f) for f in os.listdir(p) if f.endswith(LOG_EXTENSION)]
    if os.path.isfile(p):
        return [p]
    return []


if __name__ == '__main__':
    total_time = time.time()
    print(f'Requests time percentile calculator.')
    print(f'Usage:       reader-mt.py [path=<path_to_file_or_folder>]')
    print(f'Default dir: {FOLDER}')

    for f in get_files(sys.argv):
        percentile, n_bads, n_reqs, calc_time = process_file(f)
        print('-'*80)
        print(f'Filename:             {f}')
        print(f'95th percentile:      {percentile/1000000:.3f} seconds')
        print(f'Bad requests:         {n_bads} of {n_reqs}')
        print(f'Time to process file: {calc_time:.3f} seconds')
    print('-'*80)
    total_time = time.time() - total_time
    print(f'Total time:           {total_time:.3f} seconds')
    pass
