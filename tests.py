import unittest as ut
import logging
import os
import reader
import reader_mt

FOLDER = 'logs'

class MyTestCase(ut.TestCase):
    def test_compare_output(self):
        for f in [os.path.join(FOLDER,f) for f in os.listdir(FOLDER) if f.endswith('.in')]:
            print(f'Processing file {f}...')
            self.assertEqual(reader.read(f)[:3], reader_mt.process_file(f)[:3])
            print(f'File {f} passed.')

if __name__ == '__main__':
    ut.main()