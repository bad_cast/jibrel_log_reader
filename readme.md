﻿### Tool for analyzing log-files
This tool calculates percentile and bad requests number as described in `docs\Тестовое задание.pdf`

### Minimal version
Python version 3.5 or higher   
*Attention: if you are using venv under Windows, make sure Python 3.7.3 or later is installed.*  
*For more details see issue: https://bugs.python.org/issue35797*

### Installation
Type: `python3 -m pip install -r requirements.txt`

### Usage
Run `python3 reader_mt.py [path=<path_to_folder>]` for multi threaded application. Requires pandas.  
Run `python3 reader.py [path=<path_to_folder>]` for single threaded application. Requires numpy.  
Run `python3 tests.py` for tests. It compares results from `reader.py` and `reader_mt.py` functions.  
`logs` is default folder for log files. Default extension of log file is `.in`.

### Information
You can change `CHUNK_SIZE` and `CPU_COUNT` in `reader_mt.py` variables to see if perfomance would change.  
Should be tested on bigger data.   
Single threaded application is faster on small data.  
If running in IDE with debugging single threaded application is *much* slower on big data.  
