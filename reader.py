#!/usr/bin/env python

import os
import sys
import time
import numpy as np

FOLDER = 'logs'
LOG_EXTENSION = '.in'

def read(filepath):
    start_time = time.time()
    reqs = {}
    with open(filepath, 'r', encoding='utf-8') as f:
        for line in f:
            record = line.rstrip('\n').split('\t')
            if len(record) < 3:
                continue
            event = record[2]
            id = int(record[1])
            if event == 'StartRequest':
                reqs[id] = {}
                reqs[id]['bad'] = 1
                continue
            if event == 'BackendOk':
                reqs[id]['bad'] = 0
                continue
            if event == 'StartMerge':
                t = int(record[0])
                reqs[id]['merge_start'] = t
                continue
            if event == 'FinishRequest':
                if 'merge_start' in reqs[id]:
                    t = int(record[0])
                    reqs[id]['merge_time'] = t - reqs[id]['merge_start']
                continue
    # calc percentile
    percentile = np.percentile([reqs[k]['merge_time'] for k in reqs if 'merge_time' in reqs[k]], 95)
    # calc bad requests
    bad_requests, n_requests = sum([reqs[k]['bad'] for k in reqs]), len(reqs)
    time_delta = time.time() - start_time
    return [percentile, bad_requests, n_requests, time_delta]


def get_files(argv, folder=FOLDER):
    '''Check args for path. If it exists, checks if it directory or file
    '''
    p = folder
    for arg in argv:
        if arg.split('=')[0] == 'path' and len(arg) > 1:
            p = arg.split('=')[1]
            break
    if os.path.isdir(p):
        return [os.path.join(p, f) for f in os.listdir(p) if f.endswith('.in')]
    if os.path.isfile(p):
        return [p]
    return []


if __name__ == '__main__':
    total_time = time.time()
    print(f'Requests time percentile calculator.')
    print(f'Usage:       reader-mt.py [path=<path_to_file_or_folder>]')
    print(f'Default dir: {FOLDER}')
    for f in get_files(sys.argv):
        print('-'*80)
        # num_lines, num_lines_delta = get_num_lines(f)
        percentile, bad_requests, n_requests, read_delta = read(f)
        print(f'Filename:             {f}')
        # print(f'Get lines time:  {num_lines_delta:.3f}')
        print(f'95th percentile:      {percentile/1000000:.3f} seconds')
        print(f'Bad requests:         {bad_requests} of {n_requests}')
        print(f'Time to process file: {read_delta:.3f} seconds')

    print('-'*80)
    total_time = time.time() - total_time
    print(f'Time to execute:       {total_time:.3f} seconds')
